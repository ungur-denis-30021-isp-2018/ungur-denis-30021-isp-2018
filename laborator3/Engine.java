
package laborator3;


public class Engine {
            String fuellType;
            long capacity;
            boolean active;
            long power;
            Engine(int capacity,boolean active, int power){
                        this.capacity = capacity;
                        this.active = active;
                        this.power= power;
            }          
            Engine(int capacity,boolean active,int power,String fuellType){
                        this(capacity,active,power);
                        this.fuellType = fuellType;
            }          
            Engine(){
                        this(2000,false,150,"diesel");
            }       
          
            
                    void print(){
                        System.out.println("Engine: capacity="+this.capacity+" fuell="+fuellType+" active="+active+"horse power="+power);
            }
            public static void main(String[] args) {
                        Engine tdi = new Engine();
                        Engine i16 = new Engine(1600,false,300,"petrol");
                        Engine d30 = new Engine(3000,true,250,"diesel");
                        tdi.print();i16.print();d30.print();
            
            }
}
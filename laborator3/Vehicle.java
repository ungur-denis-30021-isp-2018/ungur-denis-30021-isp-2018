package laborator3;
public class Vehicle {
    String model;
    int viteza;
    String TipCombustibil;

    Vehicle(){
        model="Mercedes";
        viteza=100;
        TipCombustibil="Gaz";
    }
    Vehicle(String m,int v,String c)
    {
        model=m;
        viteza=v;
        TipCombustibil=c;
    }
    public String Model(){
        return model;

    }
    public String TipCombustibil(){
        return TipCombustibil;
    }
    public int Viteza(){
        return viteza;
    }

    public void accelerare (){
        viteza+=10;
    }
    public void franare(){
        viteza-=3;
    }
    public void stop(){
        viteza=0;
    }
    public String toString(){
        return "Modelul masini este:"+model+" viteza ei este = "+viteza+" si tipul combustibilul este "+TipCombustibil;
    }

    public static void main(String[] args) {
        Vehicle v1=new Vehicle();
        System.out.println("Modelul masini este: "+v1.Model()+" viteza este:"+v1.Viteza()+" si tipul combustibilul este: "+v1.TipCombustibil());
        Vehicle v2=new Vehicle("Dacia",45,"Benzina");
        System.out.println(v2);
        for(int i=0;i<7;i++){
            v2.accelerare();
            if (i>5)
                v2.franare();
        }
        System.out.println(v2);
        Vehicle v3=new Vehicle("Dodge",120,"Benzina");
        System.out.println(v3);
        do{
            v3.franare();
        }while(v3.viteza!=50);
        System.out.println(v3);
    }

}